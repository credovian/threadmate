"""
Pip package installation file
"""
from distutils.core import setup 

setup(
    name='threadmate',
    author='Cameron Redovian',
    description='simple api for terminal animation during process execution',
    packages=['threadmate']
)
