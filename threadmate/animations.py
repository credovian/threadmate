from time import sleep
from threading import Thread

class Roller:
  def __init__(self,task,**kw):
    self.animation = self.rolling_dots
    self.task = task
    self.name = kw.get('name','Task')
    self.run_task()

  def rolling_dots(self):
    """
    Terminal animation for long-running processes
    """
    bars = ['   ','.  ','.. ','...',' ..','  .']
    for b in bars: 
      print(f'{self.name} in progress{b}',end='\r')
      sleep(0.3)

  def run_task(self):
    """
    Display animation during task execution via multithreading
    """
    process = Thread(name=self.name,target=self.task)
    process.start()
    while process.isAlive(): 
        self.animation()
